#!/usr/bin/env bash

#https://dl.google.com/dl/android/studio/ide-zips/2.1.1.0/android-studio-ide-143.2821654-linux.zip

ANDROID_STUDIO_VERSION=2.1.1.0
ANDROID_STUDIO_BUILD=143.2821654
ANDROID_STUDIO_FILE=android-studio-$ANDROID_STUDIO_BUILD-linux.zip

#http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
ANDROID_SDK_VERSION=r24.4.1
ANDROID_SDK_FILE=android-sdk_$ANDROID_SDK_VERSION-linux.tgz

# install a better terminal emulator
sudo apt-get -y install lxterminal

# install java8
sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
sudo echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
sudo apt-get update
# accept the license agreement
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer

# make a place to install development tools
mkdir -p ~/dev/tools
cd ~/dev/tools

# download and unpack android-studio
wget https://dl.google.com/dl/android/studio/ide-zips/$ANDROID_STUDIO_VERSION/$ANDROID_STUDIO_FILE 
unzip $ANDROID_STUDIO_FILE
#rm $ANDROID_STUDIO_FILE

# create a launcher for android-studio
echo "[Desktop Entry]
Version=1.0
Type=Application
Name=Android-Studio
Comment=
Exec=/home/vagrant/dev/tools/android-studio/bin/studio.sh
Icon=/home/vagrant/dev/tools/android-studio/bin/idea.png
Terminal=false
StartupNotify=false
GenericName=" >> /home/vagrant/Desktop/Android-Studio.Desktop

# download android sdk
wget http://dl.google.com/android/$ANDROID_SDK_FILE
tar -zxf $ANDROID_SDK_FILE
#rm $ANDROID_SDK_FILE

# install android sdk extras to get google libs
ANDROID=/home/vagrant/dev/tools/android-sdk-linux/tools/android
echo y | $ANDROID update sdk --no-ui --filter extra