#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y upgrade

# get ourselves some basic toys
sudo apt-get -y install git-core build-essential openssl libssl-dev subversion
sudo apt-get -y install vim-nox gedit

# add lxde basic environment

sudo apt-get -y install lxde-core lxterminal

# install some tools for the vagrant user only by executing
# a script AS the vagrant user
cp /vagrant/install-tools.sh /home/vagrant
chown vagrant:vagrant /home/vagrant/install-tools.sh
chmod ug+x /home/vagrant/install-tools.sh
su -c "/home/vagrant/install-tools.sh" vagrant

# set up udev rules for known android device manufacturers
cat /vagrant/51-android.rules > /etc/udev/rules.d/51-android.rules
chmod a+r /etc/udev/rules.d/51-android.rules
